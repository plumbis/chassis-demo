# Chassis Demo

This repo demonstrates a [Cumulus Vx](https://cumulusnetworks.com/products/cumulus-vx/) network built with 4 facebook chassis, 6 1RU leafs and 6 dual-attached servers.


![Topology](https://gitlab.com/plumbis/chassis-demo/raw/master/topology.jpg)

The [Backpack](https://cumulusnetworks.com/blog/announcing-backpack-running-cumulus-linux/) is a 4 slot chassis. Each slot consists of 2 32 port switches, with 16 front facing ports and 16 internal, ethernet ports. There are also 4 line cards with all 32 ports acting as fabric ports. More information on the internals of the chassis can be found [on the Cumulus Networks blog](https://cumulusnetworks.com/blog/choosing-chassis/)

Just like an actual chassis, the internal ports on both the line cards and fabric cards are labeled as "fp" while the external ports are labeled as "swp".

This demo configures an eBGP unnumbered fabric within the chassis, as well as across the leafs and chassis connections. Finally, Cumulus Networks [FRR Container](https://hub.docker.com/r/cumulusnetworks/frrouting/) is deployed on every server, enabling Routing on the Host, where the servers speak eBGP to the top of rack switches.

[Gitlab-CI](https://about.gitlab.com/features/gitlab-ci-cd/) has been enabled and [Cumulus NetQ](https://cumulusnetworks.com/products/netq/) provides simple tests to verify the eBGP fabric. The NetQ Telemetry Server must be [downloaded](https://cumulusnetworks.com/downloads/#product=NetQ%20Virtual&version=1.2) and installed as a vagrant box named `cumulus/ts`. The NetQ Telemetry Server runs on the out of band management network on an independent server. The NetQ server is *not* the out of band management server.

This lab is based on Vagrant and Libvirt+KVM. The lab contains at toal of 63 nodes, so a sufficent amount of CPU and RAM is necessary on the vagrant server to launch the lab.

Given the size of the lab, it is recommened to bring up portions of the lab at a time, based on the contents of the [vagrant_up](https://gitlab.com/plumbis/chassis-demo/blob/master/vagrant_up) file.

Once the lab is launched, you can access the lab with `vagrant ssh oob-mgmt-server`. Change to the cumulus user with `sudo su - cumulus` and then jump to any node in the topology, including the chassis fabric cards. If you wish to access the NetQ Telemetry Server directly is is called `netq-ts` and can be accessed either from the oob-mgmt-server or directly with `vagrant ssh netq-ts`.
